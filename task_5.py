import asyncio
import random



from enum import Enum
from typing import Optional
from dataclasses import dataclass
from datetime import datetime, timedelta

timeout_seconds = timedelta(seconds=10).total_seconds()

class Response(Enum):
  Success = 1
  RetryAfter = 2
  Failure = 3

class ApplicationStatusResponse(Enum):
  Success = 1
  Failure = 2

@dataclass
class ApplicationResponse:
  application_id: str
  status: ApplicationStatusResponse
  description: str
  last_request_time: datetime
  retriesCount: Optional[int]


async def get_application_status1(identifier: str) -> Response:

  sleep_time = random.randint(0, 5)
  await asyncio.sleep(random.randint(0, 5))
  result = 1 if sleep_time in (2, 3) else 3 if sleep_time in (1, 4) else 2
  return Response(result)


async def get_application_status2(identifier: str) -> Response:

  sleep_time = random.randint(0, 5)
  await asyncio.sleep(sleep_time)
  result = 1 if sleep_time in (2, 3) else 3 if sleep_time in (1, 4) else 2
  return Response(result)


async def with_retry(func, idnt):
    retry_number = 0
    while 1:
        last_request = datetime.now()
        resp = await func(idnt)
        if resp == Response(2):
            retry_number += 1
            await asyncio.sleep(1)
            continue
        return ApplicationResponse(idnt, resp, f"successfull finished {func.__name__}", last_request, retry_number)


async def return_first(identifier: str) -> ApplicationResponse:

    done, pending = await asyncio.wait(
        [with_retry(get_application_status1, identifier), with_retry(get_application_status2, identifier)]
        , return_when=asyncio.FIRST_COMPLETED)
    return done.pop().result()


async def perform_operation(identifier: str) -> ApplicationResponse:
    try:
        return await asyncio.wait_for(return_first('1'), timeout=timeout_seconds)
    except asyncio.exceptions.TimeoutError:
        return ApplicationResponse(
            identifier,
            ApplicationStatusResponse(2),
            "timeout reached",
            datetime.now(),
            None
        )
        pass
    except:
        return ApplicationResponse(
            identifier,
            ApplicationStatusResponse(2),
            "unknown error",
            datetime.now(),
            None
        )

if __name__ == '__main__':
    loop = asyncio.get_event_loop()

    result = loop.run_until_complete(perform_operation('1'))
    print(result)
